/**
 * Created by Kapil on 6/25/2016.
 */
angular.module('scheduler.directives.scheduleDay',['scheduler.services'])
.directive('scheduleDay', ['schedulerService',function(schedulerService){
		return {
			restrict:'AE',
			replace:true,
			scope:{
				meetings:'='
			},
			link: function(scope, element){
				scope.meetings.forEach(function(data){
					if(isNaN(data.start) || isNaN(data.end)){
						angular.element(element).html('Configuration Error');
						throw {
							name:'InvalidDataType',
							value:'Start or End time should be number'
						}
					}
					if(data.start<0 ||data.start > 720 || data.end < 0 || data.end > 720 ){
						angular.element(element).html('Configuration Error');
						throw {
							name:'InvalidData',
							value:'Start or End time not in range'
						}
					}
					if(data.start >= data.end){
						angular.element(element).html('Configuration Error');
						throw {
							name:'InvalidTime',
							value:'meeting end time should be after meeting start time'
						}
					}
				});
				scope.gridLines = new Array(12);
				for(var i =0 ;i <scope.gridLines.length;i++){
					scope.gridLines[i] = i;
				}

				scope.meetingsWithPosition = schedulerService.schedule(scope.meetings);

			},
			template : [
				'<div class="scheduler">',
					'<div ng-repeat="meeting in meetingsWithPosition" class="meeting-box" ng-style="{ width: {{meeting.width - 2}} +\'px\' , height: {{(meeting.end - meeting.start) * 2 - 2}} +\'px\', top: {{meeting.start*2 + 1}} +\'px\', left: {{meeting.position * meeting.width}} +\'px\'}">',
						'<span>{{meeting.id}}</span>',
					'</div>',
					'<div class="grid-line" ng-repeat="line in gridLines" ng-style="{ top: {{line * 120}} +\'px\'}""></div>',
				'</div>'].join('')
		}
	}]);