/**
 * Created by Kapil on 6/25/2016.
 */
angular.module('scheduler.services',[])
.service('schedulerService', [function(){
		this.schedule = function(meetingData,totalWidth){
			if (typeof(totalWidth)==='undefined') {totalWidth = 600;}

			var sortedEvent = [];
			for(var i = 0;i<meetingData.length; i++){
				sortedEvent.push({index:i, type:'start', time:meetingData[i].start});
				sortedEvent.push({index:i, type:'end', time:meetingData[i].end});
			}
			sortedEvent.sort(function(a, b){
				if(a.time == b.time && a.type !== b.type ){
					return a.type == 'end' ? -1 : 1;
				}
				return a.time- b.time;
			});
			var groupStartIndex = 0;
			var maxParallelMeeting = 0;
			var parallelMeeting = 0;
			for(var i = 0; i < sortedEvent.length ; i++){
				if(sortedEvent[i].type === 'start'){
					if(parallelMeeting == 0 ){
						groupStartIndex = i;
						maxParallelMeeting = 0;
					}
					parallelMeeting++;
					if(parallelMeeting > maxParallelMeeting ){
						maxParallelMeeting = parallelMeeting;
					}

				}else if(sortedEvent[i].type === 'end'){
					parallelMeeting--;
				}

				if(parallelMeeting == 0){
					width = totalWidth / maxParallelMeeting;
					var position = new Array(maxParallelMeeting);
					for(var j= groupStartIndex; j < i; j++){
						if(sortedEvent[j].type === 'start'){
							meetingData[sortedEvent[j].index].width = width;
							var emptyPositionIndex = getEmptyPosition(position);
							meetingData[sortedEvent[j].index].position = emptyPositionIndex;
							position[emptyPositionIndex] = sortedEvent[j].index;
						}
						else if(sortedEvent[j].type === 'end'){
							position[getPositionOfMeeting(position, sortedEvent[j].index)] = undefined;
						}
					}
				}
			}

			return meetingData;
		};

		function getPositionOfMeeting(position, index){
			for(var k = 0 ;k<position.length; k++){
				if(position[k] == index){
					return k;
				}
			}
			return -1;
		}

		function getEmptyPosition(position){
			for(var k = 0 ;k<position.length; k++) {
				if (position[k] == undefined) {
					return k;
				}
			}
		}

		function intersect(a, b){
			return a.start < b.start && b.start <= a.end ? true : false;
		}
	}]);